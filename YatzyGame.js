let die1 = {eyes: 0, hold: false};
let die2 = {eyes: 0, hold: false};
let die3 = {eyes: 0, hold: false};
let die4 = {eyes: 0, hold: false};
let die5 = {eyes: 0, hold: false};
let values = [0,0,0,0,0];
let throwCount = 0;
let holdDice = [0,0,0,0,0];
let frequency = [0,0,0,0,0,0,0];
let tempResult = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];


function getThrowCount() {
    return throwCount;
}

function resetThrowCount() {
    throwCount = 0;
}

function getTempResult() {
    return tempResult;
}

//Sum of fields up to 6s
let sumBeforeSix = 0;

//Som of fields from one pair to yatzy
let sumAfterSix = 0;

//Used to add points to the sum
function addToSumBeforeSix(number) {
    sumBeforeSix += parseInt(number);
}

//Used to add points to the sum
function addToSumAfterSix(number) {
    sumAfterSix += parseInt(number);
}

function getSumAfterSix() {
    return sumAfterSix;
}

function getSumBeforeSix() {
    return sumBeforeSix;
}

function setTempResult(index) {
    tempResult[index] = (tempResult[index] + 1) % 2;
}

function setHoldDice(index) {
    holdDice[index] = (holdDice[index] + 1) % 2;
}

function throwDice() {
    if (getThrowCount() < 3) {
        for (let i = 0; i < values.length; i++) {
            if (!holdDice[i]) {
                values[i] = Math.floor(Math.random() * 6 + 1);
             //   let dice = document.getElementById('' + i);
             //   dice.src = "Terninger/" + values[i] + ".JPG"
            }
        }
        let img1 = document.getElementById('terning1');
        let img2 = document.getElementById('terning2');
        let img3 = document.getElementById('terning3');
        let img4 = document.getElementById('terning4');
        let img5 = document.getElementById('terning5');
    console.log(img1);
        for (let i = 0; i < values.length; i++) {
            if (i + 1 === 1) {
                img1.src = "Terninger/die" + values[i] + ".JPG"
            }
            if (i + 1 === 2) {
                img2.src = "Terninger/die" + values[i] + ".JPG"
            }
            if (i + 1 === 3) {
                img3.src = "Terninger/die" + values[i] + ".JPG"
            }
            if (i + 1 === 4) {
                img4.src = "Terninger/die" + values[i] + ".JPG"
            }
            if (i + 1 === 5) {
                img5.src = "Terninger/die" + values[i] + ".JPG"
            }

        }

        throwCount++;
        let turns = document.getElementById('TurnCounter');
        turns.innerHTML = 'Turn: ' + throwCount;
        frequency = calcCounts();
    }
}

onload = () => {
    let points = document.querySelectorAll('input');
    for(let i = 0; i < points.length; i++) {
        points[i].values =  getResults()[i];
    }

};

function getResults() {
    let results = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    for (let i = 0; i <= 5; i++) {
        results[i] = sameValuePoints(i+1);
    }
    results[6] = onePairPoints();
    results[7] = twoPairPoints();
    results[8] = threeSamePoints();
    results[9] = fourSamePoints();
    results[10] = fullHousePoints();
    results[11] = smallStraightPoints();
    results[12] = largeStraightPoints();
    results[13] = chancePoints();
    results[14] = yatzyPoints();

    return results;
}

function calcCounts() {
    let counts = [0,0,0,0,0,0,0];
    for(let i = 0; i < values.length; i++) {
        counts[values[i]]++;
    }
    return counts;
}

function sameValuePoints(value) {
    return frequency[value] * value;
}

function compareNums(number, index) {
    let result;
    for (let i = index; i > 0; i--) {
        if (frequency[i] >= number) {
            result = i*number;
            return result;
        }
    }
    return result;
}
    
function onePairPoints() {
    return compareNums(2,6);
}

function twoPairPoints() {
    let result = compareNums(2,6);
    if (result >= 4) {
        let secondPair = compareNums(2, (result / 2) - 1);
        if (secondPair !== 0) {
            result += secondPair;
            return result;
        }
    }
    return 0;
}


function threeSamePoints() {
    return compareNums(3, 6);
}


function fourSamePoints() {
    return compareNums(4, 6);
}



function fullHousePoints() {
    let frequency = calcCounts();
    let twoAlike = 0;
    let threeAlike = 0;
    for (let i = 1; i < frequency.length; i++) {
        if (frequency[i] === 3) {
            threeAlike = i * 3;
        } else if (frequency[i] === 2) {
            twoAlike = i * 2;
        }
        if (twoAlike !== 0 && threeAlike !== 0) {
            return twoAlike + threeAlike;
        }
    }
    return 0;
}

function smallStraightPoints() {
    for (let i = 1; i < frequency.length-1; i++) {
        if (frequency[i] !== 1) {
            return 0;
        }
    }
    return 15;
}
    
function largeStraightPoints() {
    for (let i = 2; i < frequency.length; i++) {
        if (frequency[i] !== 1 ) {
            return 0;
        }
    }
    return 20;
}

function chancePoints() {
    let chance = 0;
    for (let i = 0; i < values.length; i++) {
        chance += values[i];
    }
    return chance;
}

function yatzyPoints() {
    let result = 0;
    for (let i = 1; i < frequency.length; i++) {
        if (frequency[i] === 5) {
            result = 50;
        }
    }
    return result;
}

console.log(throwDice());