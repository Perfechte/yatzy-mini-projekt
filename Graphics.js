let initialisation = 0;

function initialise() {
    setHandlers();
    setHandlersOnButton();
    initialisation++;
}

//function rollAvailDice(values) {
//    document.getElementById('TurnCounter').innerText = 'Turn' + getThrowCount();
//   while (!holdDice) {

//    for (let i = 0; i < values.length; i++) {
//        let filetype = '.JPG';
//        document.getElementById('Terninger/die' + (i + 1)).src = 'Terninger/die' + values[i] + filetype;
//   }
//   updateFields();
//}}


function updateFields() {
    let simpleSum = 0;
    for(let i = 0; i<fieldIds.length; i++) {
        let results = getResults();
        if(tempResult[i] === 0) {
            let onesinput = document.getElementById(fieldIds[i]);
            onesinput.value = results[i];
        }
    }
    if(initialisation == 0) {
        initialise();
    }
}

function setHandlersOnButton() {
    for(let i = 1; i<=values.length;i++){
        document.getElementById((i) + "dice").addEventListener("click", function() {
            if(getThrowCount() > 0){
                setHoldDice(i-1);
                let element = document.getElementById((i) + "dice");
                if(holdDice[i-1]===1){
                    element.style.backgroundColor = "brown";
                } else {
                    element.style.backgroundColor = "";
                }
            }
        }, false);
    }
}

function resetHoldDice() {
    document.getElementById("1dice").style.backgroundColor="";
    document.getElementById("2dice").style.backgroundColor="";
    document.getElementById("3dice").style.backgroundColor="";
    document.getElementById("4dice").style.backgroundColor="";
    document.getElementById("5dice").style.backgroundColor="";

    for(let i = 0; i<holdDice.length;i++){
        holdDice[i] = 0;
    }
}

function setHandlers() {
    for(let i = 0; i < fieldIds.length; i++){
        let element = document.getElementById(fieldIds[i]);
        element.addEventListener("click", function() {
            console.log("res:" + tempResult[i]);
            if(getThrowCount() > 0 && tempResult[i] === 0){

                element.style.backgroundColor = "brown";
                element.style.color="white";
                setTempResult(i);
                element.readOnly = true;


                if(i < 6){
                    addToSumBeforeSix(element.value);
                }
                let onesinput = document.getElementById("input-first-sum");
                onesinput.value = getSumBeforeSix();


                if(i >= 6){
                    addToSumAfterSix(element.value);
                }
                let othersinput = document.getElementById("Yahtzy");
                othersinput.value = getSumAfterSix();


                if(getSumBeforeSix() >= 63){
                    let onesinput = document.getElementById("input-first-sum");
                    onesinput.value = 50;
                }


                let upperSum = isNaN(parseInt(document.getElementById("input-first-sum").value))?0:document.getElementById("input-first-sum").value;
                let upperBonus = isNaN(parseInt(document.getElementById("input-bonus").value))?0:document.getElementById("input-bonus").value;
                let lowerSum = isNaN(parseInt(document.getElementById("input-final-sum").value))?0:document.getElementById("input-final-sum").value;


                let total = document.getElementById("input-total");
                total.value = parseInt(upperSum) + parseInt(lowerSum)+parseInt(upperBonus);


                resetThrowCount();
                resetHoldDice();
                document.getElementById("turnCount").innerHTML = "Turn " + getThrowCount();
            }
        });
    }
}

